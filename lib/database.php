<?php
//Se declara la clase de la Base de datos para la conexion
class Database
{
    //Se declara la variable con la que se hara la conexion
    private static $connection;
    private static $statement;
    public static $id;
    public static $error;

    //Se declara la funcion para realizar la conexion
    private static function connect()
    {
        //Se declaran variables que tomas los datos de la base de datos
        $server = "localhost";
        $database = "paramedicos";
        $username = "gonzza";
        $password = "hola";
        $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "set names utf8");     
        try
        {
            @self::$connection = new PDO("mysql:host=".$server."; dbname=".$database, $username, $password, $options);
        }
        catch(PDOException $exception)
        {
            throw new Exception($exception->getCode());
        }
    }
    //Funcion para cerrar la conexion
    private static function desconnect()
    {
        self::$connection = null;
    }
    public static function executeRow($query, $values)
    {
        self::connect();
        //Ejecuta una sentencia preparada
        $statement = self::$connection->prepare($query);
        $state = $statement->execute($values);
        self::$id = self::$connection->lastInsertId();
        self::desconnect();
        return $state;
    }
    //Funcion para obtener registros
    public static function getRow($query, $values)
    {
        self::connect();
        //Ejecuta una sentencia preparada
        $statement = self::$connection->prepare($query);
        $statement->execute($values);
        self::desconnect();
        return $statement->fetch(PDO::FETCH_BOTH);
    }
    //Funcion para obtener registros
    public static function getRows($query, $values)
    {
        self::connect();
        //Ejecuta unna setencia preparada
        $statement = self::$connection->prepare($query);
        $statement->execute($values);
        self::desconnect();
        return $statement->fetchAll(PDO::FETCH_BOTH);
    }
}
?>