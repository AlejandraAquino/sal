<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Paramedicos</title>

  <!--llamando al CSS  --> 
  <link href="../css/icon.css" rel="stylesheet">
  <link href="../css/columnas.css" rel="stylesheet">
  <link href="../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>

  <div>

      <?php
    session_start();
    if(isset($_SESSION['cliente']))
    {
        include('../inc/menu2.php');
    }
    else
    {
        include('../inc/menu.php');
    }
    ?>


    <!--Empieza cuerpo d e la pagina-->
  
 <div class="parallax-container">
      <div class="parallax"><img src="../img/port.png"></div>
    </div>
     <div class="row">
        <div class="col s12 m6">
          <div class="card lighten-3 blue accent-2">
            <div class="card-content white-text">
              <span class="card-title">Visión<i class="tiny material-icons left">subject</i></span>
              <p>Ratificarnos como la empresa más confiable, rentable, y competitiva en el suministro de equipos médicos y productos del sector de salud en El Salvador, mediante la constante búsqueda de la excelencia, a través del mejoramiento continuo de nuestro recurso humano, activo fundamentalmente de paramédicos, y así asegurar a nuestros clientes, proveedores y colaboradores un servicio de altura.</p>
            </div>
          </div>
        </div>
           <div class="row">
        <div class="col s12 m6 right">
          <div class="card lighten-3 blue accent-2">
            <div class="card-content white-text">
              <span class="card-title">Misión<i class="tiny material-icons left">subject</i></span>
              <p>Compromiso fundamental con nuestros clientes, proveedores y colaboradores, de proporcionarles la más alta calidad en servicio y tecnología en nuestros equipos con los precios más competitivos en productos y servicios para el cuidado de la salud a nivel nacional, con el fin de conservar o recuperar la calidad de la salud, colocándolos al alcance de quienes lo necesitan donde se requiere y en el momento oportuno.</p>
            </div>
          </div>
        </div>
      </div> 
      </div>
    <div class="parallax-container">
      <div class="parallax"><img src="../img/portada.jpg"></div>
    </div>
    <div class="center-aling">
    <h5 class="center black-text">Marcas que nos identifican:</h5>
      <code>
         <a class="center-aling"><img src="../img/marca5.gif" width="350" height="120"></a>
         <a class="center-aling"><img src="../img/marca3.jpg" width="400" height="120"></a>
         <a class="center-aling"><img src="../img/marca2.png" width="350" height="120"></a>
         <a class="center-aling"><img src="../img/marca4.png" width="350" height="120"></a>
         <a class="center-aling"><img src="../img/marca1.png" width="400" height="120"></a>
         <a class="center-aling"><img src="../img/marca6.png" width="350" height="120"></a>
      </code>
      </div>
  <!--Pie de la pagina-->
<?php include("../inc/pie.php");?>
</div>
 <!--  Scripts-->
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.min.js"></script>
  <script src="../js/iniciar.js"></script>
  <script>
  $( document ).ready(function(){
    $('.parallax').parallax();
    $('.modal').modal();
    $('#modal1').modal('open');
 $('#modal1').modal('close');
 $('.slider').slider();
})
</script>
</body>
</html>