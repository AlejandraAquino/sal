<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Paramedicos</title>

  <!--llamando al CSS  --> 
  <link href="../css/icon.css" rel="stylesheet">
  <link href="../css/columnas.css" rel="stylesheet">
  <link href="../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <div>
<!--Menú del inicio-->

      <?php
    session_start();
    if(isset($_SESSION['cliente']))
    {
        include('../inc/menu2.php');
    }
    else
    {
        include('../inc/menu.php');
    }
    ?>


    <!--Empieza cuerpo de la pagina-->
   
    <div class="parallax-container">
    <div class="parallax"><img src="../img/2.png" ></div>
    <div class="caption center-align">
          <h2 class="light black-text text-lighten-3">Paramedic</h2>
          <h4 class="light black-text text-lighten-3">Los mejores en lo que hacemos.</h4>
        </div>
  </div>
  <div class="section blue lighten-5 ">
    <div class="row container blue lighten-5">
      
      <p class="grey-text text-darken-3 white">
       <div class="row">
     <div class="col s5">
        <!-- Promo Content 3 goes here -->
      <img src="../img/333.png"width="300" height="250"></div>
      <div class="col s7 "align="justify">
        <!-- Promo Content 3 goes here -->
      
      Paramedicos El Salvador se fundó en el año de 1994. Surgió como una iniciativa de Juan Hidalgo y Freddy Majano,
        quienes iniciaron su sueño en 1994 junto a Jesús Hidalgo, padre de Juan Hidalgo. En un principio,
         la organización fue inscrita como una sociedad dedicada al servicio de equipos de oftalmología
          y mantenimiento de microscopios. Cinco años más tarde, la empresa paramedicos El Salvador
           se une al empresario austríaco Erick Quezada, quien ya administraba su propia empresa (Semi-Lab),
            también especialista en implementos médicos.

“La unión de estas dos empresas nos permitió el acceso a un capital de trabajo más fuerte, ampliando el áreas de servicio,
 ha llevado a posicionarnos como una de las empresas de mejor prestigio en esta rama a nivel centroamericano”, aseguró Juan Hidalgo</div>
    </div>
       
      
      </p>
    </div>
  </div>
  
   <div class="parallax-container ">
    <div class="parallax"><img src="../img/5.jpg"></div>
    <div class="caption center-align">
          <h2 class="light black-text text-lighten-3">Marcas</h2>
          <h4 class="light black-text text-lighten-3">Mas Reconocidas .</h4>
        </div>
  </div>
  <div class="section blue lighten-3">
    <div class="row container blue lighten-3">
      
      <p class="grey-text text-darken-3 lighten-3">
       <div class="row">
   <P></p>

      <div class="col s4" align="justify">
      <img src="../img/h.png" width="250" height="250">
        Tuttnauer es un fabricante de autoclaves,
        un productor de esterilizadores de plasma
        y un proveedor de lavadoras desinfectadoras
        térmicas, así como de otros productos para
        el control de las infecciones en las industrias
        sanitarias y biológicas. Desde hace más de 89 años,
      </div>
      <div class="col s4" align="justify">
      <img src="../img/mel.png" width="250" height="250">
        Medela, con sede en cantón de tren / Suiza fue fundada
        en 1961 por Olle Larsson. La empresa está dirigida por
        su hijo Michael Larsson hoy. Medela tiene dos divisiones,
        "Leche Humana" para el desarrollo y fabricación de productos
        y soluciones fijas, y "Cuidado de la Salud",
      </div>
      <div class="col s4" align="justify">
      <img src="../img/am.png" width="250" height="229">
      <p>
        las ideas innovadoras han impulsado nuestro
        trabajo en aportar soluciones eficientes de salud para
        la vida. Esto es lo que creamos en nuestras áreas de
        negocio - Anestesia, Monitorización & Diagnóstico y
        Emergencias. Millones de pacientes y profesionales
        sanitarios en todo el mundo dependen de la funcionalidad
        y el rendimiento de nuestros productos.
        </p>
      </div>
      

    </div>
      
      </p>
    </div>
  </div>
  <div class="parallax-container">
    <div class="parallax"><img src="../img/4.png"></div>
  </div>
   
  
          
  <!--Pie de la pagina-->
<?php include("../inc/pie.php");?>
</div>
 <!--  Scripts-->
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.min.js"></script>
  <script src="../js/iniciar.js"></script>
 <script>
  $( document ).ready(function(){
    $(".button-collapse").sideNav();
    $('.parallax').parallax();
    $('.modal').modal();
      $('#modal1').modal('open');
      $('#modal1').modal('close');
       $('.slider').slider();
})
</script>
</body>
</html>