<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Paramedicos</title>

  <!--llamando al CSS  --> 
  <link href="../css/icon.css" rel="stylesheet">
  <link href="../css/columnas.css" rel="stylesheet">
  <link href="../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <div>
<!--Menú del inicio-->
<nav class="top-nav  light-blue darken-2">
        <div class="container">
          <div class="nav-wrapper center"><a class="page-title">Iniciar Sesión</a></div>
        </div>
      </nav>
      <?php
    session_start();
    if(isset($_SESSION['cliente']))
    {
        include('../inc/menu2.php');
    }
    else
    {
        include('../inc/menu.php');
    }
    ?>



    <!--Empieza cuerpo de la pagina-->
    <div id="medio" class="col s12 m9 l10">
 <div class="parallax-container valign-wrapper"><!--Efecto de las imagenes con texto-->
      <div class="parallax"><img src="../img/cardio.jpg" alt="Unsplashed background img 2"></div>
    <div class="row">
      <div class="col s20 m30">
        <div class="card-panel teal">
          <span class="white-text">
            <div class="row center">
              <span>
              <div class="row">
              <div class="input-field col s15">
                <input id="last_name" type="text" class="validate">
                <label for="last_name" class="black-text text-black">Usuario</label>
              </div>
            <div class="row">
              <div class="input-field col s15">
                <input id="password" type="password" class="validate">
                <label for="password" class="black-text text-black">Contraseña</label>
              </div>
            </div>
              </span>
                </div>
              </div>
            </span>
        </div>
      </div>
    </div>
            </div>
        <pre>
            <h6 class="center"><i class="material-icons Medium">new_releases</i> Si aun no tienes cuenta <a class="blue-text text-blue" href="#!">Registrate</a></h6>
            <h6 class="right"> Uso exclusivo para clinicas y hospitales de El Salvador</h6>
        </pre>
 <!--Pie de la pagina-->
<?php include("../inc/pie.php");?>

 <!--  Scripts-->
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.min.js"></script>
  <script src="../js/iniciar.js"></script>
</body>
</html>