<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Paramedicos</title>

  <!--llamando al CSS  --> 
  <link href="../css/icon.css" rel="stylesheet">
  <link href="../css/columnas.css" rel="stylesheet">
  
  <link href="../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body style="background-color:grey lighten-3;">
  <div>

      <?php
    session_start();
    if(isset($_SESSION['cliente']))
    {
        include('../inc/menu2.php');
    }
    else
    {
        include('../inc/menu.php');
    }
    
    ?>


    <!--Empieza cuerpo de la pagina-->
    <div class="slider">
    <ul class="slides">
      <li>
        <img src="../img/5.jpg"> <!-- random image -->
        <div class="caption center-align">
          <h3>Imagen</h3>
          <h5 class="light grey-text text-lighten-3">Paramedicos.</h5>
        </div>
      </li>
      <li>
        <img src="../img/2.png"> <!-- random image -->
        <div class="caption left-align">
          <h3>Imagen</h3>
          <h5 class="light grey-text text-lighten-3">Paramedicos.</h5>
        </div>
      </li>
      <li>
        <img src="../img/3.jpg"> <!-- random image -->
        <div class="caption right-align">
          <h3>Imagen</h3>
          <h5 class="light grey-text text-lighten-3">Paramedicos.</h5>
        </div>
      </li>
      <li>
        <img src="../img/4.png"> <!-- random image -->
        <div class="caption center-align">
          <h3>Imagen</h3>
          <h5 class="light grey-text text-lighten-3">Paramedicos.</h5>
        </div>
      </li>
    </ul>
  </div>


  <div class="section grey darken-3"  style="color:#ffffff";>
    <div class="row container center-align">
    <div class="col s12 m6 l3"><i class="tiny material-icons ">check_circle</i><b> CON LICENCIA OFICIAL</b></div>
    <div class="col s12 m6 l3"><i class=" tiny material-icons">pan_tool</i><b>CONFIDENCIALIDAD</b></div>
    <div class="col s12 m6 l3"><i class="tiny material-icons">stars</i><b>Productos 100% Seguros</b></div>
    <div class="col s12 m6 l3"><i class="tiny material-icons">work</i><b>Mas de 16 años de Servicios</b></div>

  </div>
 </div>
 <div class='container' id='productos'>
 <h4 class='center-align'>NUESTROS PRODUCTOS</h4>
		<div class='row'>
       <div class="container">
  <form method='post' >
    <div class="row">
        <div class='input-field col s4 offset-s4 '>
			<span> </span>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar Por Nombre </label>
		</div>
        <div class='input-field col s4 '>
        <button type='submit' class='btn waves-effect green'><i class='material-icons'>search</i></button> 	
	    </div>
    </div>
</form>
 <div class="row">
    <?php
    require("../lib/database.php");
    if(!empty($_POST)) //If para validar si esta vacio el textbox
    {
	$search = trim($_POST['buscar']); //Quita los espacios vacios del name buscar
	$sql = "SELECT * FROM productos WHERE nombre_producto LIKE ?  ORDER BY nombre_producto"; //consulta con el like que contenga lo que se pone en el buscador
	$params = array("%$search%"); //se manda como parametro el array y la consulta
    }
    else
    {
        require('../lib/zebra_pagination.php');
        $total = "SELECT COUNT(*) FROM productos";
        $resultados = 1;
        $paginacion= new Zebra_Pagination;
        $paginacion->records($total);
        $paginacion->records_per_page($resultados);

        //$sql = 'SELECT * FROM productos, proveedores WHERE productos.Id_Proveedores = proveedores.Id_Proveedores AND Estado_Producto = 1 LIMIT' .(($paginacion->get_page()-1)*$resultados).','.$resultados;
        $sql = 'SELECT * FROM productos, distribucion WHERE productos.id_marca = distribucion.id_marca AND Estado_Producto = 1 ';
       //$sql .= "LIMIT ".(($pagination->get_page()-1)*$resultados).",".$resultados;
        $params = null;
        

    }
    try
    {
        $data = Database::getRows($sql, $params);
        if($data != null)
        {
            foreach($data as $row)
            {
                print("
                        <div class='col s12 m4 l4'>
            <div class='card medium'>
                <div class='card-image waves-effect waves-block waves-light'>
                  <img class='activator' src='data:image/*;base64,$row[imagen_producto]'>
                </div>
                <div class='card-content'>
                    <span class='card-title activator grey-text text-darken-4'><span class='icon-info'></span> $row[nombre_producto]</span>
                    <a href='../dashboard/main/detalle.php?id=".$row['id_producto']."' class='waves-effect waves-light btn green darken -3'>Informacion Producto</a>
                </div>
                <div class='card-reveal'>
                    <span class='card-title grey-text text-darken-4'> $row[nombre_producto]</span>
                    <p>Precio: $row[precio_producto]</p>
                </div>
            </div>
        </div>   
                ");
            }
          
        }
        
        else
        {
            print("<div class='card-panel yellow'>No hay registros disponibles en este momento.</div>");
        }
    }
    catch(Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), "../main/");
    }
		?>
		</div><!-- Fin de row -->
	</div><!-- Fin de container -->
  <?php include("../inc/pie.php");?>
</body>
 <!--  Scripts-->
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.min.js"></script>
  <script src="../js/iniciar.js"></script>
  <script>
  $( document ).ready(function(){
    $(".button-collapse").sideNav();
    $('.parallax').parallax();

    
    $('.modal').modal();
    $('#modal1').modal('open');
 $('#modal1').modal('close');
 $('.slider').slider();
 
})
</script>

</html>