<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Paramedicos</title>

  <!--llamando al CSS  --> 
  <link href="../css/icon.css" rel="stylesheet">
  <link href="../css/columnas.css" rel="stylesheet">
  <link href="../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <div>
<!--Menú del inicio-->

      <?php
    session_start();
    if(isset($_SESSION['cliente']))
    {
        include('../inc/menu2.php');
    }
    else
    {
        include('../inc/menu.php');
    }
    ?>

    <!--Empieza cuerpo de la pagina-->
    <div >
 <div class="parallax-container">
      <div class="parallax"><img src="../img/distri.png"></div>
    </div>
    <div class="col s12 m7">
      <h2 class="header  grey darken-3 white-text center">Encuentranos en</h2>
        <div class="card horizontal">
          <div class="card-image">
            <img src="../img/el_salvador1.png">
        </div>
        <div class="card-stacked">
          <div class="card-content">
            <p>Prolongación Calle Arce Edificio #3020 entre 57 y 39 Av.Norte San Salvador, El Salvador C.A.</p>
        </div>
        <div class="card">
          <a><pre>
          Tels: 2124-6872/73                                        E-mail: paraemdicos.vtas@integra.com.sv 
                2265-2099                                                   gerencia@paramedicos.com.sv
          Fax: (503) 2208-4973
          </pre>
          </a>
          </div>
      </div>
    </div>
  </div>
<pre>
 <h4> Ubicación:</h4>
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d873.3768547426027!2d-89.22063952880531!3d13.700960416270911!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9e8ea5eacab732d0!2sDragon+Chino.!5e0!3m2!1ses!2ssv!4v1490003869004" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
 </pre>

  <!--Pie de la pagina-->
<?php include("../inc/pie.php");?>
</div>
 <!--  Scripts-->
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.min.js"></script>
  <script src="../js/iniciar.js"></script>
</body>
</html>