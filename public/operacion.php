<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Paramedicos</title>

  <!--llamando al CSS  --> 
  <link href="../css/icon.css" rel="stylesheet">
  <link href="../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<!--Menú del inicio-->
 <?php
    session_start();
    if(isset($_SESSION['cliente']))
    {
        include('../inc/menu2.php');
    }
    else
    {
        include('../inc/menu.php');
    }
    ?>

 <div class="container">
 		<div class="row">
 			<div class="col s12 ">
 				<div class="slider">
    <ul class="slides">
      <li>
        <img src="../img/2.png"> <!-- random image -->
        <div class="caption center-align">
          <h3>This is our big Tagline!</h3>
          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
        </div>
      </li>
      <li>
        <img src="../img/3.jpg"> <!-- random image -->
        <div class="caption jleft-align">
          <h3>Left Aligned Caption</h3>
          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
        </div>
      </li>
      <li>
        <img src="../img/4.png"> <!-- random image -->
        <div class="caption right-align">
          <h3>Right Aligned Caption</h3>
          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
        </div>
      </li>
      <li>
        <img src="../img/5.jpg"> <!-- random image -->
        <div class="caption center-align">
          <h3>This is our big Tagline!</h3>
          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
        </div>
      </li>
    </ul>
  </div>
 			</div>
 		</div>
 	</div>
  
  	<div class="section blue lighten-2">
    <div class="row container center-align">
     <div class="col s12 m6 l3"><i class="tiny material-icons">check_circle</i><b> CON LICENCIA OFICIAL</b></div>
    <div class="col s12 m6 l3"><i class=" tiny material-icons">pan_tool</i><b>CONFIDENCIALIDAD</b></div>
    <div class="col s12 m6 l3"><i class="tiny material-icons">stars</i><b>Productos 100% Seguros</b></div>
    <div class="col s12 m6 l3"><i class="tiny material-icons">work</i><b>Mas de 16 años de Servicios</b></div>


    </div>
  </div>



  <ul class="pagination center-align">
    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
    <li class="active"><a href="#!">1</a></li>
    <li class="waves-effect"><a href="#!">2</a></li>
    <li class="waves-effect"><a href="#!">3</a></li>
    <li class="waves-effect"><a href="#!">4</a></li>
    <li class="waves-effect"><a href="#!">5</a></li>
    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
  </ul>
<div class="container blue lighten-5 col s12">
    <div class="row">
      <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/fifa.jpg"></a>
             
          </div>
          <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a  href="juego1.php" class="waves-effect waves-light btn ">Informacion</a>
           

           
          </div>
        </div>
      </div>
      <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/bat.png" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego2.php" class="waves-effect waves-light btn">Informacion</a>
           
    </div>
  </div>
   </div>
   <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/five.jpg" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego3.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
  <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/god.jpg" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego4.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
  <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/halo.jpg" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego5.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
  <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/nba.jpg" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego6.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
  <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/call.png" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego7.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
  <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/dead.jpg" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego8.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
  <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/gta.jpg" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a  href="juego9.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
  <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/over1.jpg" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego10.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
  <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/plan.jpg" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego11.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
  <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/res.jpg" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego12.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
   <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/es.png" ></a>
            
            </div>
             <div class="card-content  blue lighten-2 ">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego13.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
   <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/zomm.jpg" ></a>
            
            </div>
             <div class="card-content  blue lighten-2">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego14.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
  <div class="col s4">
        <div class="card">
          <div class="card-image">
           <a href="#!"> <img src="../img/portadas/es.png" ></a>
            
            </div>
             <div class="card-content  blue lighten-2 ">
            <img src="../img/win_blanco.png" align="left" width="25" height="25" >
            <img src="../img/apple_gris.ico" align="left" width="25" height="25" >
           <a href="juego13.php" class="waves-effect waves-light btn">Informacion</a>
  </div>
  </div>
  </div>
  
  
  
  
  
 
 


























 
 
  
  
  
   <!--Pie de la pagina-->
<!--- ?php include("../inc/pie.php");?-->
 <!--  Scripts-->
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.min.js"></script>
  <script src="../js/iniciar.js"></script>
  <script type="text/javascript">
var bits=100;var intensity=15;var speed=15;var colours=new Array("#f44336");
var dx, xpos, ypos, bangheight;var Xpos=new Array();var Ypos=new Array();var dX=new Array();var dY=new Array();var decay=new Array();var colour=0;var swide=800;var shigh=600;function write_fire() {var b, s;b=document.createElement("div");s=b.style;s.position="absolute";b.setAttribute("id", "bod");document.body.appendChild(b);set_scroll();set_width();b.appendChild(div("lg", 3, 4));b.appendChild(div("tg", 2, 3));for (var i=0; i<bits; i++) b.appendChild(div("bg"+i, 1, 1));}function div(id, w, h) {var d=document.createElement("div");d.style.position="absolute";d.style.overflow="hidden";d.style.width=w+"px";d.style.height=h+"px";d.setAttribute("id", id);return (d);}function bang() {var i, X, Y, Z, A=0;for (i=0; i<bits; i++) {X=Math.round(Xpos[i]);Y=Math.round(Ypos[i]);Z=document.getElementById("bg"+i).style;if((X>=0)&&(X<swide)&&(Y>=0)&&(Y<shigh)) {Z.left=X+"px";Z.top=Y+"px";}if ((decay[i]-=1)>14) {Z.width="3px";Z.height="3px";}else if (decay[i]>7) {Z.width="2px";Z.height="2px";}else if (decay[i]>3) {Z.width="1px";Z.height="1px";}else if (++A) Z.visibility="hidden";Xpos[i]+=dX[i];Ypos[i]+=(dY[i]+=1.25/intensity);}if (A!=bits) setTimeout("bang()", speed);}
function stepthrough() {var i, Z;var oldx=xpos;var oldy=ypos;xpos+=dx;ypos-=4;if (ypos<bangheight||xpos<0||xpos>=swide||ypos>=shigh) {for (i=0; i<bits; i++) {Xpos[i]=xpos;Ypos[i]=ypos;dY[i]=(Math.random()-0.5)*intensity;dX[i]=(Math.random()-0.5)*(intensity-Math.abs(dY[i]))*1.25;decay[i]=Math.floor((Math.random()*16)+16);Z=document.getElementById("bg"+i).style;Z.backgroundColor=colours[colour];Z.visibility="visible";}bang();launch();}document.getElementById("lg").style.left=xpos+"px";document.getElementById("lg").style.top=ypos+"px";document.getElementById("tg").style.left=oldx+"px";document.getElementById("tg").style.top=oldy+"px";}function launch() {colour=Math.floor(Math.random()*colours.length);xpos=Math.round((0.5+Math.random())*swide*0.5);ypos=shigh-5;dx=(Math.random()-0.5)*4;bangheight=Math.round((0.5+Math.random())*shigh*0.4);document.getElementById("lg").style.backgroundColor=colours[colour];document.getElementById("tg").style.backgroundColor=colours[colour];}window.onscroll=set_scroll;function set_scroll() {var sleft, sdown;if (typeof(self.pageYOffset)=="number") {sdown=self.pageYOffset;sleft=self.pageXOffset;}else if (document.body.scrollTop || document.body.scrollLeft) {sdown=document.body.scrollTop;sleft=document.body.scrollLeft;}else if (document.documentElement && (document.documentElement.scrollTop || document.documentElement.scrollLeft)) {sleft=document.documentElement.scrollLeft;sdown=document.documentElement.scrollTop;}else {sdown=0;sleft=0;}var s=document.getElementById("bod").style;s.top=sdown+"px";s.left=sleft+"px";}window.onresize=set_width;function set_width() {if (typeof(self.innerWidth)=="number") {swide=self.innerWidth;shigh=self.innerHeight;}else if (document.documentElement && document.documentElement.clientWidth) {swide=document.documentElement.clientWidth;shigh=document.documentElement.clientHeight;}else if (document.body.clientWidth) {swide=document.body.clientWidth;shigh=document.body.clientHeight;}}window.onload=function() { if (document.getElementById) {set_width();write_fire();launch();setInterval('stepthrough(3)', speed);}}
</script>
<script>
$( document ).ready(function(){
    $('.slider').slider();
 $(".button-collapse").sideNav();
 $('.parallax').parallax();
 
})
</script>
</body>
</html>