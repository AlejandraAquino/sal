<div class="navbar-fixed">
 <nav>
    <div class="nav-wrapper  blue lighten-2">
      <a href="index.php" class="brand-logo"> <img src="../img/333.png"width="90" height="65"></a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="../public/productos.php"><i class='material-icons left'>web</i>Inicio</a></li>
        <li><a href="../public/index.php"><i class='material-icons left'>reorder</i>Mision y Vision</a></li>
        <li><a href="../public/producs.php"><i class='material-icons left'>shop</i>Productos</a></li>
        <li><a href="../public/contactenos.php"><i class='material-icons left'>phone</i>Contactenos</a></li>
        <a class="btn tooltipped light-blue green darken -3" data-position="bottom" data-delay="50" data-tooltip="En Hora Buena" href="../dashboard/main/subdetalle.php"><i class='material-icons'>shopping_cart</i></a>
        <a class="btn tooltipped light-blue black" data-position="bottom" data-delay="50" data-tooltip="Home" href="../dashboard/main/index.php"><i class='material-icons'>perm_identity</i></a>
      </ul>
      
      <ul class="side-nav" id="mobile-demo">
        <li><a href="../public/productos.php">Inicio</a></li>
        <li><a href="../public/index.php">Mision y Vision</a></li>
        <li><a href="../public/producs.php">Productos</a></li>
        <li><a href="../public/contactenos.php">Contactenos</a></li>
        <a class="btn tooltipped light-blue yellow" data-position="bottom" data-delay="50" data-tooltip="En Hora Buena" href="../dashboard/main/login.php">Carrito</a>
            <a class="btn tooltipped light-blue  blue lighten-5" data-position="bottom" data-delay="50" data-tooltip="Home" href="../dashboard/main/index.php">Cuenta</a>
      </ul>
      </div>
      </nav>
   </div>