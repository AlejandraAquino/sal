<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../lib/page.php");
Page::header("Bienvenid@");
?>
<!-- Se obtiene la fecha, dia y año-->
<div class="row">
	<h4 class='center-align'>Hoy es <?php print(date('d/m/Y')); ?></h4>
</div>

<?php
Page::footer();
?>