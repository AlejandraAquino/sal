			<!DOCTYPE html>
			<html lang='es'>
			<head>
				<meta charset='utf-8'>
				<title>Dashboard - $title</title>
				<link type='text/css' rel='stylesheet' href='../../css/sweetalert2.min.css'/>
				<script type='text/javascript' src='../../js/sweetalert2.min.js'></script>
				<link type='text/css' rel='stylesheet' href='../../css/materialize.min.css' media='screen,projection' />
  			    <link type='text/css' rel='stylesheet' href='css/style.css' />
                <link type='text/css' rel='stylesheet' href='../../fonts/francoisone/novaslim.css' />
				<link type='text/css' rel='stylesheet' href='../../fonts/style.css' />
				<script type='text/javascript' src='../../js/sweetalert2.min.js'></script>
				<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
				<title> Xsensation360 </title>
			</head>
			<body>


<?php
require("../lib/page.php");
if(!empty($_GET['id']))
{
    $id = $_GET['id'];
}
else
{
    header("location: subdetalle.php");
}

if(!empty($_POST))
{
	$id = $_POST['id'];
	try 
	{
		$sql = "DELETE FROM ventas WHERE Id_Venta = ?";
	    $params = array($id);
	    if(Database::executeRow($sql, $params))
		{
			 $mensaje = "Se Elimino del Carrito";
	   		 Page::showMessage(1, $mensaje, "subdetalle.php");
		}
		else
		{
			throw new Exception(Database::$error[1]);
		}
	}
	catch (Exception $error) 
	{
		Page::showMessage(2, $error->getMessage(), "subdetalle.php");
	}
}
?>
<form method='post'>
	<div class='row center-align'>
		<input type='hidden' name='id' value='<?php print($id); ?>'/>
		<button type='submit' class='btn waves-effect red'><span class="icon-point-down"> Borrar</span></button>
		<a href='subdetalle.php' class='btn waves-effect grey'><span class="icon-point-down"> Cancelar</span></a>
	</div>
</form>
</body>
</html>