<?php
require("../lib/page2.php");
Page::header("Detalle");

    $id = $_GET['id'];
    $sql = "SELECT * FROM productos, distribucion WHERE productos.id_marca = distribucion.id_marca AND id_producto='$id'";
    $data = Database::getRow($sql, null);
    $nombre = $data ['nombre_producto'];
    $precio = $data['precio_producto'];
    $cantidad = $data['cantidad'];
    


    try
    {
             print("
             <div class= 'container'>
             <div class = 'row'>
             
             <div class= 'col s6'>
                <ul class='collection with-header'>
                    <li class='collection-header'><h4>$nombre<a class='secondary-content'><span class='icon-newspaper'></span></a></h4></li>
                    <li class='collection-item'>Precio: $precio<a class='secondary-content'><span class='icon-price-tag'></span></a></li>
                    <li class='collection-item'>Cantidad Disponible: $cantidad <a class='secondary-content'><span class='icon-stack'></span></a></li>
                    <li class='collection-item'>Pago: Cuando se Entrege <a class='secondary-content'><span class='icon-credit-card'></span></a></li>
                    <li class='collection-item'> Envio: Domicilio <a class='secondary-content'><span class='icon-location'></span></a></li>
                </ul>
            </div>");
            
                if(isset($_POST['action']))
                {
                    
                    $_POST = validator::validateForm($_POST);
                    $fecha = date('Y/m/d'); //varibale para guardar la fecha
                    $estado = 1; //estado de la factura, es 1 por que no se ha pagado
                    $cantidad = 1; //cantidad por defecto 1
                    try
                    {
                        if(isset($_SESSION['compra'])) //si ya existe una factura
                        {
                                $sql1 = "SELECT Id_Factura FROM factura WHERE Id_Cliente = ? ORDER By Id_Factura DESC LIMIT 1";
                                $params1 = array($_SESSION['Id_Cliente']);
                                $data1 = Database::getRow($sql1, $params1);
                                $idfactura = $data1['Id_Factura'];
                                $sql="INSERT INTO ventas(id_producto, Cantidad_Venta, Id_Factura) VALUES(?,?,?)";
                                $params =array($id,$cantidad,$_SESSION['compra']);
                                if(Database::executeRow($sql, $params))
                                {
                                    Page::showMessage(1, "Se te Agrego a tu Carrito De Compras", "");
                                    $_SESSION['compra'] = $data1['Id_Factura'];
                                }
                                else
                                {
                                    throw new Exception(Database::$error[1]);
                                }
                        }
                        else
                        {
                            $sql2 = "INSERT INTO factura(Id_Cliente, Fecha_Factura,Estado_Factura) VALUES(?, ?, ?)";
                            $params2= array($_SESSION['Id_Cliente'],$fecha,$estado);
                            if(Database::executeRow($sql2, $params2))
                            {
                                $sql1 = "SELECT Id_Factura FROM factura WHERE Id_Cliente = ? ORDER By Id_Factura DESC LIMIT 1";
                                $params1 = array($_SESSION['Id_Cliente']);
                                $data1 = Database::getRow($sql1, $params1);
                                $idfactura = $data1['Id_Factura'];
                                $sql="INSERT INTO ventas(id_producto,Cantidad_Venta, Id_Factura) VALUES(?,?,?)";
                                $params =array($id,$cantidad,$idfactura);
                                if(Database::executeRow($sql, $params))
                                {
                                    Page::showMessage(1, "Se creo un Carrito de Compras", "");
                                    $_SESSION['compra'] = $data1['Id_Factura'];
                                }
                                else
                                {
                                    throw new Exception(Database::$error[1]);
                                }
                                
                            }
                            else
                            {
                                throw new Exception(Database::$error[1]);
                            }
                        }
                    }
                    catch(Exception $error)
                    {
                        Page::showMessage(2, $error->getMessage(), null);
                    }
                }

              print("  <div class='col s2'>
                <form  method='post'>   
                ");
                    if(isset($_SESSION['cliente']))
                    {
                        if(isset($_POST['action']))
                        {
                            echo "<button class='btn disabled waves-effect waves-light green darken -3' type='submit' name='action'>Agregar al Carrito</button>";                            
                        }
                        else
                        {
                            print("
                        <a href='../dashboard/main/detalle.php?id=$id'>
                            <button class='btn waves-effect waves-light green darken -3' type='submit' name='action'>Agregar al Carrito</button>
                        </a>
                        ");
                        }
                    }
                print("
                    
                </form>
                </div>
            </div>
            </div>
                ");

                if(isset($_SESSION['cliente']))
                {
                    $sql3 = "SELECT p.nombre_producto FROM ventas v, factura f, productos p, clientes c WHERE f.Estado_Factura= ? AND v.id_producto= p.id_producto AND f.Id_Factura=v.Id_Factura AND f.Id_Cliente=? AND p.id_producto= ? GROUP BY v.Id_Venta ";
                    $est = 0;
                    $params3 = array($est,$_SESSION['Id_Cliente'], $id);
                    
                    $data3 = Database::getRows($sql3, $params3);
                    if($data3 != null)
                    {
                        if(isset($_POST['publicar']))
                        {
                             
                             $_POST = Validator::validateForm($_POST);
                             $comentario = $_POST['comentario'];
                             
                            try
                            { 
                                if($comentario !="")
                                {                                                    
                                    $sql4 = "INSERT INTO comentario(comentario, id_producto, Id_Cliente) VALUES(?, ?, ?)";
                                    $params4 = array($comentario,$id,$_SESSION['Id_Cliente']);
                                    if(Database::executeRow($sql4, $params4))
                                    {
                                        Page::showMessage(1, "Gracias por Comentar", "../../index.php");
                                    }
                                    else
                                    {
                                        throw new Exception(Database::$error[1]);
                                    }
                                }
                                else
                                {
                                    throw new Exception("Llene el campo para comentar");
                                }
                            }
                            catch (Exception $error)
                            {
                                Page::showMessage(2, $error->getMessage(), null);
                            }
                        }

                         ?>
                            <div class='container'>
                                <div class='row'>
                                    <form method='post'>
                                    <div class='input-field col s10'>
                                        <textarea id='textarea1' name='comentario' class='materialize-textarea' data-length='120'></textarea>
                                        <label for='textarea1'>Dinos Tu Opinion</label>
                                    </div>
                                    <div class='col s2'>
                                        <button class='btn waves-effect waves-light green darken -3' type='submit' name='publicar'>Comentar</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <?php
                            $sql7 ="SELECT * FROM valoracion WHERE valoracion.Id_Cliente=? AND valoracion.id_producto=?";
                            $params7 = array($_SESSION['Id_Cliente'],$id);
                            $data7 = Database::getRow($sql7, $params7);
                            if($data7==null)
                            {
                                                            ?>
                             <form action="#" method='post'>
                                <div class='container'>
                                    <div class='row'>
                                        <div class='col s4' >
                                            <p class="range-field">
                                                Califica Tu Producto<input type="range" id="test5" min="0" max="10" name="califica"/>
                                            </p> 
                                        </div>
                                        <div class='col s2'>
                                            <button class='btn waves-effect waves-light green darken -3' type='submit' name='valorar'>Calificar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <?php                
                                if(isset($_POST['valorar']))
                                {
                                    $_POST = Validator::validateForm($_POST);
                                    $califica = $_POST['califica'];
                                    
                                    try
                                    {
                                        $sql6 = "INSERT INTO valoracion(Id_Cliente,id_producto, Valoracion) VALUES(?,?,?)";
                                        $params6 = array($_SESSION['Id_Cliente'], $id, $califica);
                                        if(Database::executeRow($sql6, $params6))
                                        {
                                            Page::showMessage(1, "Gracias por Valorarme ", "../../public/productos.php");
                                        }
                                        else
                                        {
                                            throw new Exception(Database::$error[1]);
                                        }
                                    }
                                    catch (Exception $error)
                                    {
                                        Page::showMessage(2, $error->getMessage(), null);
                                    }
                                }
                            }
                            else
                            {
                                $sql8 = "SELECT COUNT(*) as total FROM valoracion WHERE id_producto = ?";//Cuenta cuantas valoraciones hay para un X producto
                                $params8 = array($id);
                                $data8 = Database::getRow($sql8, $params8);
                                $total = $data8['total'];
                                $sql9 = "SELECT SUM(Valoracion) as valoracion FROM valoracion WHERE id_producto = ?"; //suma las valoraciones para un producto x
                                $params9 = array($id);
                                $data9 = Database::getRow($sql9, $params9);
                                $sumva = $data9['valoracion']; 
                                $promedio = $sumva/$total; //se saca el promedio
                                $prom = round($promedio,2);
                                print ("<div class='container'><h4> Valoracion:$prom </h4> </div>");
                            }
                    }
                }
                $sql5 = "SELECT comentario.comentario, clientes.Usuario_Cliente FROM comentario, clientes, productos WHERE comentario.Id_Cliente=clientes.Id_Cliente AND comentario.id_producto=? GROUP BY comentario.id_comentario";
                $params5=array($_GET['id']);
                $data5 = Database::getRows($sql5, $params5);
                print ("<div class= 'container'><h4>Comentarios </h4></div>");
            if($data5!=null)
            {
                foreach($data5 as $rowe) //para mostrar el numero de comentarios X.
                {   print("
                        <div class='container'>
                          <div class='row'>
                            <div class='input-field col s12'>
                            <input disabled value='".$rowe['comentario']."' id='disabled' type='text' class='validate'>
                            <label for='disabled'>Usuario: ".$rowe['Usuario_Cliente']."</label>
                            </div>
                          </div>
                        </div>
                        ");
                }
            }
            else
            {
                print("<div class='container'><h3>No hay comentarios Aun</h3></div>");
            }
    }
    catch(Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), "../main/");
    }
Page::footer();
?>

 