<?php
require("../lib/page2.php");
Page::header("Registrate");
if(!empty($_POST))
{
    $_POST = validator::validateForm($_POST);
    $usuario = $_POST['usuario'];
    $nombres= $_POST['nombres'];
    $apellidos = $_POST['apellidos'];
    $email = $_POST['email'];
    $email1 = $_POST['email1'];
    $clave = $_POST['clave'];
    $clave1 = $_POST['clave1'];
    $telefono = $_POST['telefono'];
    $direccion = $_POST['direccion'];
    try
    {
        if($usuario != "" && $direccion !="")
        {
            if($nombres != "" && preg_match("|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|",$nombres))
            {
                if($apellidos != "" && preg_match("|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|",$apellidos))
                {
                    if($email != ""&& Validator::validateEmail($email))
                    {
                        if($email1 != "")
                        {
                            if($clave != "")
                            {
                                if($clave1 != "")
                                {
                                    if($telefono != "" && strlen($telefono)==8 && is_numeric($telefono))
                                    {
                                        if($email == $email1)
                                        {
                                            if($clave == $clave1)
                                            {
                                                if(isset($_POST['condiciones']))
                                                {                              
                                                    $sql = "INSERT INTO clientes(Usuario_Cliente, Clave_Cliente, Correo_Cliente, 
                                                    Nombres_Cliente, Apellidos_Cliente, Telefono_Cliente, Direccion_Cliente) VALUES(?, ?, ?, ?, ?, ?, ?)";
                                                    $params = array($usuario,$clave,$email,$nombres,$apellidos,$telefono,$direccion);
                                                    if(Database::executeRow($sql, $params))
                                                    {
                                                        Page::showMessage(1, "Operación satisfactoria", "login.php");
                                                    }
                                                    else
                                                    {
                                                        throw new Exception(Database::$error[1]);
                                                    }
                                                }
                                                else
                                                {
                                                    throw new Exception("Acepte Terminos y Condiciones");
                                                }
                                            }
                                             else
                                            {
                                                throw new Exception("Las contraseñas no coinciden");
                                            }
                                        }
                                        else
                                        {
                                            throw new Exception("Los Correos no coinciden");
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("Ingrese un Telefono Correcto");
                                    }
                                }
                                else
                                {
                                    throw new Exception("Vuelva a escribir la clave");
                                }
                            }
                            else
                            {
                                throw new Exception("Escriba una clave");
                            }
                        }
                        else
                        {
                            throw new Exception("Escriba de nuevo el correo");
                        }
                    }
                    else
                    {
                        throw new Exception("Ingres un correo");
                    }
                }
                else
                {
                    throw new Exception("Escriba Sus Apellidos, que sean validos");
                }
            }
            else
            {
                throw new Exception("Escriba sus Nombres, ques sean validos");
            }
        }
        else
        {
            throw new Exception("Escriba un Usuario o Direccion");
        }
    }
    catch(Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
else
{
    $usuario = null;
    $nombres= null;
    $apellidos = null;
    $email = null;
    $email1 = null;
    $telefono = null;
    $direccion = null; 
}

?>
    
    <div class="container">
        <div id="bueno" class="row">
            <form class="col s12" method='post'>
                <!--Formulario de nombre y apellido -->
                <div class="row">
                    <div class="input-field col s6">
                        <input id="user" type="text" class="validate" name="usuario" value='<?php print($usuario); ?>' required/>
                        <label for="user">Usuario</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="first_name" type="text" class="validate" name="nombres" value='<?php print($nombres); ?>' required/>
                        <label for="first_name">Nombre</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="last_name" type="text" class="validate" name="apellidos" value='<?php print($apellidos); ?>' required/>
                        <label for="last_name">Apellidos</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="email" type="email" class="validate" name="email" value='<?php print($email); ?>' required/>
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="email1" type="email" class="validate" name="email1" value='<?php print($email1); ?>' required/>
                        <label for="email1">repita email</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="password" type="password" class="validate" name="clave">
                        <label for="password">Contraseña</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="password" type="password" class="validate" name="clave1">
                        <label for="password">Repita su Contraseña</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="icon_telephone" type="tel" class="validate" name="telefono" value='<?php print($telefono); ?>' required/>
                        <label for="icon_telephone"> <span class="icon-old-phone"></span> Teléfono </label>
                    </div>
                    <div class="input-field col s12">
                        <input id="last_name" type="text" class="validate" name="direccion" value='<?php print($direccion); ?>' required/>
                        <label for="last_name">Direccion</label>
                    </div>
                </div>
            
                <p>
                    <input type="checkbox" id="test5" name='condiciones' value='v1' />
                    <label center for="test5">Acepta Terminos y condiciones</label>
                </p>
                <button class="btn waves-effect waves-light" type="submit">Unirme!<span class="icon-redo2"></span></button>       
            </form>
        </div>
    </div>


    <!--aqui comienza el footer-->
<?php
Page::footer();
?>
