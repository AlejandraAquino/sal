<?php
require("../lib/page.php");
Page::header("Mis Compras");
?>
<?php
    $sql = "SELECT f.Id_Factura, f.Fecha_Factura, p.nombre_producto, p.precio_producto, v.Cantidad_Venta FROM ventas v, factura f, productos p, clientes c WHERE f.Estado_Factura= ? AND v.id_producto= p.id_producto AND f.Id_Factura=v.Id_Factura AND f.Id_Cliente=? GROUP BY v.Id_Venta";
    $es = 0; //varibale que solo mostraran las comrpas que si se compraron
    $params = array($es,$_SESSION['Id_Cliente']);
    $data = Database::getRows($sql, $params);
    try
    {
        
        if($data != null)
        {
            print("
            <div class='container'>
<table class='striped' >
	<thead>
		<tr>
			<th>No Factura</th>
			<th>Fecha</th>
			<th>Productos</th>
			<th>Precio</th>
            <th>Cantidad</th>
		</tr>
	</thead>
	<tbody>
");
            foreach($data as $row)
            {
                        print("
			<tr>
				<td>".$row['Id_Factura']."</td>
				<td>".$row['Fecha_Factura']."</td>
				<td>".$row['nombre_producto']."</td>             
				<td>".$row['precio_producto']."</td>
                <td>".$row['Cantidad_Venta']."</td>
			</tr>
		            ");
            }
                	print("
                </tbody>
            </table>
            </div>
            ");
        }
        else
        {
        Page::showMessage(4, "No has realizado compras", "../../index.php");
        }
    }
    catch(Exception $error)
{
	Page::showMessage(2, $error->getMessage(), "../main/");
}
Page::footer();
?>
