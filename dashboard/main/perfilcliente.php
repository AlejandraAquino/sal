<?php
require("../lib/page.php");
Page::header("Editar Perfil");

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombre'];
  	$apellidos = $_POST['apellido'];
    $usuario = $_POST['usuario'];
    $correo = $_POST['correo'];
    $telefono = $_POST['telefono'];
    $clave1 = $_POST['clave'];
    $clave2 = $_POST['clave1'];
    $direccion = $_POST['direccion'];
    $compotel = 8;
   
    try
    {
        if($nombres !="" && $apellidos !="" &&  preg_match("|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|",$nombres)  && preg_match("|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|",$apellidos))
        {
            if($usuario !="")
            {
                if($correo !="" && Validator::validateEmail($correo))
                {
                    if($telefono !="" && $direccion != "" && strlen($telefono)==$compotel && $telefono>=0 && is_numeric($telefono))
                    {
                        if($clave1 != "" || $clave2 != "")
                        {
                            if($clave1 == $clave2)
                            {
                                $sql = "UPDATE clientes SET Usuario_Cliente = ?,  Clave_Cliente = ?, Correo_Cliente= ?, Nombres_Cliente = ?, Apellidos_Cliente = ?, Telefono_Cliente = ?, Direccion_Cliente = ? WHERE Id_Cliente = ?";
                                $params = array($usuario,$clave1,$correo,$nombres, $apellidos,$telefono,$direccion, $_SESSION['Id_Cliente']);
                               
                            }
                            else
                            {
                                throw new Exception("Contraseña no coinciden");
                                
                            }
                        }
                        else
                        {
                               $sql = "UPDATE clientes SET Usuario_Cliente = ?,  Correo_Cliente= ?, Nombres_Cliente = ?, Apellidos_Cliente = ?, Telefono_Cliente = ?, Direccion_Cliente = ? WHERE Id_Cliente = ?";
                                $params = array($usuario,$correo,$nombres, $apellidos,$telefono,$direccion, $_SESSION['Id_Cliente']);
                        }
                        if(Database::executeRow($sql, $params))
                        {
                        Page::showMessage(1, "Operación satisfactoria", "../main/index.php");
                        }
                        else
                        {
                         throw new Exception(Database::$error);
                        }

                    }
                    else
                    {
                        throw new Exception("Debe Ingresar un Telefono valido y direccion valida");
                    }
                }
                else
                {
                    throw new Exception("Ingresa un correo");
                }
            }
            else
            {
                throw new Exception("Ingresa un usuario");
            }
        }
        else
        {
           throw new Exception("Ingresa nombre y apellido");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
else
{
    
    $sql = "SELECT  Usuario_Cliente , Correo_Cliente, Nombres_Cliente, Apellidos_Cliente , Telefono_Cliente , Direccion_Cliente FROM clientes WHERE Id_Cliente = ? ";
     $params = array($_SESSION['Id_Cliente']);
    $data = Database::getRow($sql, $params);
	$nombres = $data['Nombres_Cliente'];
  	$apellidos = $data['Apellidos_Cliente'];
    $usuario = $data['Usuario_Cliente'];
    $correo = $data['Correo_Cliente'];
    $telefono = $data['Telefono_Cliente'];
    $direccion = $data['Direccion_Cliente'];

}
?>
    <div class="container">

    <div class="row">
    <form class="col s12" method='post'>
      <div class="row">
        <div class="input-field col s12 m3">
          <input  id="first_name" type="text" class="validate" name="nombre" value='<?php print($nombres); ?>'>
          <label for="first_name">Nombre</label>
        </div>
        <div class="input-field col s12 m3">
          <input id="last_name" type="text" class="validate" name="apellido" value='<?php print($apellidos); ?>'>
          <label for="last_name">Apellido</label>
        </div>
        <div class="input-field col s12 m3">
          <input id="last_name" type="text" class="validate" name="usuario" value='<?php print($usuario); ?>'>
          <label for="last_name">Usuario</label>
        </div>
      
        <div class="input-field col s12 m3">
          <input id="email" type="email" class="validate" name="correo" value='<?php print($correo); ?>'>
          <label for="email">Email</label>
        </div>
        <div class="input-field col s12 m3">
          <input id="last_name" type="text" class="validate" name="telefono" value='<?php print($telefono); ?>'>
          <label for="last_name">Telefono</label>
        </div>
        <div class="input-field col s12 m3">
          <input id="last_name" type="text" class="validate" name="clave">
          <label for="last_name">Contraseña</label>
        </div>
        <div class="input-field col s3">
          <input id="password" type="text" class="validate" name="clave1">
          <label for="password"> Repita Contraseña</label>
        </div>
        <div class="input-field col s12 m3">
         <textarea id="textarea1" class="materialize-textarea" name="direccion" ><?php print($direccion); ?></textarea>
          <label for="textarea1">Direccion</label>
        </div>
        <div class='row center-align'>
        <button type='submit' class='btn waves-effect blue'>guardar</button>
        </div>
      </div>
    </form>
    </div>
    </div>

<?php
Page::footer();
?>
