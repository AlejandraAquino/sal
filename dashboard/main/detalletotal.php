<?php
require("../lib/page2.php");
Page::header("Mi Carrito de Compras");
    $sql = "SELECT ventas.Id_Venta, productos.id_producto, productos.nombre_producto, ventas.Cantidad_Venta, productos.precio_producto, productos.cantidad FROM ventas, productos WHERE ventas.id_producto = productos.id_producto AND ventas.Id_Factura=?";
    $sql3= "SELECT SUM(ventas.Cantidad_Venta*productos.precio_producto) AS TOTAL FROM ventas, productos WHERE ventas.id_producto = productos.Id_producto AND ventas.Id_Factura=?";
    //la consulta de arriba sql3 sirve para sacar la suma total
    $params = array($_SESSION['compra']);
    $data1 = Database::getRow($sql3, $params);//se ejecuta la consulta 3
    $data = Database::getRows($sql, $params);
    $total = $data1['TOTAL']; //se guarda el resultado.

    
try
{
    if($data!=null)
    {
        print
        ("
        <div class= 'container'>
        <table class='striped centered'>
                <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio Unitario</th>
                        <th>Subtotal</th>                     
                    </tr>
                </thead>
		    <tbody>
        ");
         $xdd = 0;
        foreach($data as $row)
        {
            print
            
            ("
                <tr>
                    <td>".$row['nombre_producto']."</td>
                    <td>".$row['Cantidad_Venta']."</td>
                    <td>".$row['precio_producto']."</td>
                    <td>");
                    $xd =  $row['precio_producto']* $row['Cantidad_Venta'];
                    print($xd);
                    print("</td>
            ");
        }
        print("
                </tr>
		    </tbody>
	    </table>
        <p>TOTAL ($) : "); print($total); print("</p>
        </div>");
       
    }
    else
    {
        Page::showMessage(4, "No has agregado productos", "../../index.php");
    }
}
catch(Exception $error)
{
	Page::showMessage(2, $error->getMessage(), "../main/");
}
?>
<?php
   $estad= 0;
    if(!empty($_POST))
    {
        $_POST = Validator::validateForm($_POST);
        try
        {   
            $estad= 0;//varible de la factura, el 0 es que ya se pago
            $sql4 = "UPDATE factura SET Estado_Factura = ? WHERE Id_Factura=  ?";
            $params4= array($estad,$_SESSION['compra']);
            if(Database::executeRow($sql4, $params4))
            {
                foreach($data as $row)// Sirve para actualizar los productos existentes
                {
                  $ac = $row['cantidad'] - $row['Cantidad_Venta'];
                  $pro = $row['id_producto'];
                  $sql5 = "UPDATE productos SET cantidad= ? WHERE id_producto = ? ";
                  $params5 = array($ac,$pro);
                  Database::executeRow($sql5, $params5);
                }
                Page::showMessage(1, "Gracias por Comprar", "index.php");
                $_SESSION['compra'] = null;
            }
            else
            {
                throw new Exception(Database::$error[1]);
            }
        }
        catch (Exception $error)
        {
            Page::showMessage(2, $error->getMessage(), null);
        }
    }
?>




<div class='container'>
    <form method='post'>
    <a href='subdetalle.php' class='btn waves-effec white-text'><span class='icon-arrow-left'></span> Regresar </a>
    <button class="btn waves-effect waves-light green" type="submit" name="action"><span class='icon-coin-dollar'></span> Pagar
    </button>
    </form>
</div>
<?php
Page::footer();
?>
