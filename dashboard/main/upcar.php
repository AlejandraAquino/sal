<!DOCTYPE html>
<html lang='es'>
    <head>
        <meta charset='utf-8'>
        <title>Dashboard - $title</title>
        <link type='text/css' rel='stylesheet' href='../../css/sweetalert2.min.css'/>
        <script type='text/javascript' src='../../js/sweetalert2.min.js'></script>
        <link type='text/css' rel='stylesheet' href='../../css/materialize.min.css' media='screen,projection' />
        <link type='text/css' rel='stylesheet' href='../../css/style.css' />
        <link type='text/css' rel='stylesheet' href='../../fonts/francoisone/novaslim.css' />
        <link type='text/css' rel='stylesheet' href='../../fonts/style.css' />
        <script type='text/javascript' src='../../js/sweetalert2.min.js'></script>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
        <title> Paramedicos </title>
    </head>
<body>
<?php
require("../lib/page.php");
if(empty($_GET['id'])) 
{
    header("location: subdetalle.php");
}
else
{
    $id = $_GET['id'];
    $sql= "SELECT ventas.Id_Venta, productos.nombre_producto, ventas.Cantidad_Venta, productos.cantidad FROM ventas, productos WHERE Id_Venta= ? AND ventas.id_producto=productos.id_producto";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $i = $data['Id_Venta'];
    $c = $data['cantidad'];
    $d = $data['Cantidad_Venta'];
}


if(!empty($_POST))
{

        $_POST = Validator::validateForm($_POST);
        $cant = $_POST['precio'];
        try
        {
            if($cant != "")
            {
                if($cant<=$c) //si la cantidad que se elijio es mejor a la cnatidad de existencias entra.
                {
                    $sql1 = "UPDATE ventas SET Cantidad_Venta = ? WHERE Id_Venta = ?";
                    $params1 = array($cant,$id);
                    if(Database::executeRow($sql1, $params1))
                    {
                        Page::showMessage(1, "Se Actualizo la Cantidad", "subdetalle.php");
                    }
                    else
                    {
                        throw new Exception(Database::$error[1]);
                    }
                }
                else
                {
                    throw new Exception("Debe ser menos Cantidad");
                }
            }
        }
        catch (Exception $error)
        {
        Page::showMessage(2, $error->getMessage(), null);
        }
}
    
?>
    <div >
    <div class="container" style='padding-top:100px;'>
    <p> Ingresa la cantidad Que deseas comprar de este producto </p>
    <form method='post'>
        <div class='row center-align'>
            <div class='input-field col s12 m6'>
                <input id='precio' type='number' name='precio' class='validate' max='<?php print($c); ?>' min='1'  value='<?php print($d); ?>' required/>
                <label for='precio'><?php $c ?></label>
            </div>
            <input type='hidden' name='id' value='<?php print($id); ?>'/>
            <button type='submit' class='btn waves-effect green'>Actualizar</button>
            <a href='subdetalle.php' class='btn waves-effect grey'><span class="icon-point-down"> Cancelar</span></a>
        </div>
    </form>
    </div>
    <div>
</body>
</html>