<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../lib/page.php");
Page::header("Iniciar sesión");

$sql = "SELECT * FROM administrador";
$data = Database::getRows($sql, null);
//Si no existen usuarios registrados te envia al formulario de registro
if($data == null)
{
		header("location: register.php");
}

if(!empty($_POST))
{
	//Se validan los datos
	$_POST = validator::validateForm($_POST);
  	$alias = $_POST['alias'];
  	$clave = $_POST['clave'];
  	try
    {
		
      	if($alias != "" && $clave != "")
  		{
  			$sql = "SELECT * FROM administrador WHERE alias = ?";
			$sql1 = "SELECT * FROM clientes WHERE Usuario_Cliente = ?";

		    $params = array($alias);
			$params1 = array($alias);
		    $data = Database::getRow($sql, $params);
			$data1 = Database::getRow($sql1, $params1);
		    if($data != null)
		    {
		    	$hash = $data['clave'];
		    	if(password_verify($clave, $hash)) 
		    	{
			    	$_SESSION['id_admin'] = $data['id_admin'];
			      	$_SESSION['nombre_usuario'] = $data['nombre_admin']." ".$data['apellido_admin'];
			      	Page::showMessage(1, "Autenticación correcta", "index.php");
				}
			}
				else 
				{
					if($data1 != null)
		        {
                    $sql= "SELECT * FROM clientes WHERE Usuario_Cliente = ?  and Estado_Cliente = 2";
                    $params = array($alias);
                    $data = Database::getRow($sql, $params);
                    if($data == null)
                    {
                        throw new Exception("Tu Usuario ha sido bloqueado");
                    }
                    else
                    {
			    	$_SESSION['Id_Cliente'] = $data1['Id_Cliente'];
			      	$_SESSION['cliente'] = $data1['Nombres_Cliente']." ".$data1['Apellidos_Cliente'];
					Page::showMessage(1, "Autenticación correcta", "index.php");
                    }
		         }
				
                else
                 {
					throw new Exception("La clave ingresada es incorrecta");
				 }
				}
	         }
	else
	  	{
	    	throw new Exception("Debe ingresar un alias y una clave");
	  	}
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

<!--Botones y cuadros de texto-->
<form method='post'>
	<div class='row'>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>person_pin</i>
			<input id='alias' type='text' name='alias' class='validate' required/>
	    	<label for='alias'>Usuario</label>
		</div>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>security</i>
			<input id='clave' type='password' name='clave' class="validate" required/>
			<label for='clave'>Contraseña</label>
		</div>
	</div>
	<div class='row center-align'>
		<button type='submit' class='btn waves-effect'><i class='material-icons'>send</i></button>
	</div>
	<div class="chip"> 
                    <li><a class="black-text text-lighten-3" href="nuevoregistro.php">¿Aun No tienes cuenta? Registrate</a>
                    </li>
                    </div>
	
</form>

<?php
Page::footer();
?>