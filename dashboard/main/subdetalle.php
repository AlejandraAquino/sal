<?php
require("../lib/page2.php");
Page::header("Mi Carrito de Compras");
    $sql = "SELECT ventas.Id_Venta, productos.nombre_producto, ventas.Cantidad_Venta, productos.precio_producto, productos.cantidad FROM ventas, productos WHERE ventas.id_producto = productos.id_producto AND ventas.Id_Factura=?";
	$params = array($_SESSION['compra']);
    $data = Database::getRows($sql, $params);

    
try
{
    if($data!=null)
    {
        print
        ("
        <div class= 'container'>
        <table class='striped centered'>
                <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio Unitario</th>
                        <th>Quitar</th>
                        <th>Modificar</th>
                        
                    </tr>
                </thead>
		    <tbody>
        ");
        
        foreach($data as $row)
        {
            print
            
            ("
                <tr>
                    <td>".$row['nombre_producto']."</td>
                    <td>".$row['Cantidad_Venta']."</td>
                    <td>".$row['precio_producto']."</td>
                    <td>
                        <a href='delcar.php?id=".$row['Id_Venta']."' class='blue-text'><span class='icon-bin'></span>Eliminar</a>
                    </td>
                     <td>
                        <a href='upcar.php?id=".$row['Id_Venta']."' class='blue-text'><span class='icon-spinner3'></span>Editar</a>
                    </td>
            ");
            
        }
        print("
                </tr>
		    </tbody>
	    </table>
          <a href='detalletotal.php' class='btn waves-effec white-text'><span class='icon-coin-dollar'></span>Pagar</a>      
        </div>
	    ");     
    }
    else
    {
        Page::showMessage(4, "No has agregado productos", "../../index.php");
    }
}
catch(Exception $error)
{
	Page::showMessage(2, $error->getMessage(), "../main/");
}

?>
<?php
Page::footer();
?>
