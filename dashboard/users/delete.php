<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../lib/page.php");
// Se coloca el titulo
Page::header("Eliminar usuario");

if(!empty($_GET['id'])) 
{
	//Se obtiene el identificador
    $id = $_GET['id'];
}
else
{
    header("location: index.php");
}

if(!empty($_POST))
{
	$id = $_POST['id'];
	try 
	{
		//Si esta con sesion abierta no se puede eliminar a el mismo
		if($id != $_SESSION['id_usuario'])
		{
			$sql = "DELETE FROM usuarios WHERE id_usuario = ?";
		    $params = array($id);
			//Si se realiza la consulta
		    if(Database::executeRow($sql, $params))
            {
                Page::showMessage(1, "Operación satisfactoria", "index.php");
            }
            else
            {
				throw new Exception("Operación fallida");
            }
		}
		else
		{
			throw new Exception("No se puede eliminar a sí mismo");
		}
	} 
	catch (Exception $error) 
	{
		Page::showMessage(2, $error->getMessage(), "index.php");
	}
}
?>
<!--Alerta-->
<form method='post'>
	<div class='row center-align'>
		<input type='hidden' name='id' value='<?php print($id); ?>'/>
		<button type='submit' class='btn waves-effect red'><i class='material-icons'>remove_circle</i></button>
		<a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
	</div>
</form>

<?php
Page::footer();
?>