<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../lib/page.php");
if(empty($_GET['id'])) 
{
    Page::header("Agregar usuario");
    //Se declaran la variables
    $id = null;
    $nombres = null;
    $apellidos = null;
    $correo = null;
    $alias = null;
    $genero = null;
    $nacion = null;
    $estado = null;
}
else
{
    Page::header("Modificar usuario");
     //Se obtiene el Id del dato guardado
    $id = $_GET['id'];
    $sql = "SELECT * FROM usuarios WHERE id_usuario = ?";
    $params = array($id);
    //Se obtienen los datos de todas la columnas
    $data = Database::getRow($sql, $params);
    $nombres = $data['nombres'];
    $apellidos = $data['apellidos'];
    $correo = $data['correo'];
    $alias = $data['alias'];
    $genero = $data['genero'];
    $nacion = $data['nacion'];
    $estado = $data['estado_usuario'];
}

if(!empty($_POST))
{
    //Se validan los datos
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombres'];
  	$apellidos = $_POST['apellidos'];
    $correo = $_POST['correo'];
    $genero = $data['genero'];
    $nacion = $data['nacion'];
    $estado = $data['estado'];

    try 
    {
      	if($nombres != "" && $apellidos != "")
        {
            //Se valida el correo
            if(Validator::validateEmail($correo))
            {
                if($id == null)
                {
                    $alias = $_POST['alias'];
                    if($alias != "")
                    {
                        $clave1 = $_POST['clave1'];
                        $clave2 = $_POST['clave2'];
                        if($clave1 != "" && $clave2 != "")
                        {
                            if($clave1 == $clave2)
                            {
                                $clave = password_hash($clave1, PASSWORD_DEFAULT);
                                //consulta para guardar los registros
                                $sql = "INSERT INTO usuarios(nombres, apellidos, correo, alias, clave, genero, nacion, estado_usuario) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
                                //Se ponen los parametros
                                $params = array($nombres, $apellidos, $correo, $alias, $clave, $genero, $nacion, $estado);
                            }
                            else
                            {
                                throw new Exception("Las contraseñas no coinciden");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe ingresar ambas contraseñas");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar un alias");
                    }
                }
                else
                {
                     //consulta para actualizar los registros
                    $sql = "UPDATE usuarios SET nombres = ?, apellidos = ?, correo = ?, genero = ?, nacion = ?, estado_usuario = ? WHERE id_usuario = ?";
                    //Se ponen los parametros
                    $params = array($nombres, $apellidos, $correo, $genero, $nacion, $estado, $id);
                }
                if(Database::executeRow($sql, $params))
                {
                    Page::showMessage(1, "Operación satisfactoria", "index.php");
                }
                else
                {
                    throw new Exception("Operación fallida");
                }
            }
            else
            {
                throw new Exception("Debe ingresar un correo electrónico válido");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<!-- Botones y cuadros de texto -->
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombres' type='text' name='nombres' class='validate' value='<?php print($nombres); ?>' required/>
          	<label for='nombres'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos' type='text' name='apellidos' class='validate' value='<?php print($apellidos); ?>' required/>
            <label for='apellidos'>Apellidos</label>
        </div>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='correo' type='email' name='correo' class='validate' value='<?php print($correo); ?>' required/>
            <label for='correo'>Correo</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='alias' type='text' name='alias' class='validate' <?php print("value='$alias' "); print(($id == null)?"required":"disabled"); ?>/>
            <label for='alias'>Alias</label>
        </div>
        <div class='input-field col s12 m6'>
            <span>Genero:</span>
            <input id='activo1' type='radio' name='genero' class='with-gap' value='1' <?php print(($genero == 1)?"checked":""); ?>/>
            <label for='activo1'><span>M</span></label>
            <input id='inactivo1' type='radio' name='genero' class='with-gap' value='0' <?php print(($genero == 0)?"checked":""); ?>/>
            <label for='inactivo1'><span>F</span></label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>my_location</i>
            <input id='nacion' type='text' name='nacion' class='validate' value='<?php print($nacion); ?>' required/>
            <label for='nacion'>Nacion</label>
        </div>
        <div class='input-field col s12 m6'>
            <span>Estado:</span>
            <input id='activo' type='radio' name='estado' class='with-gap' value='1' <?php print(($estado == 1)?"checked":""); ?>/>
            <label for='activo'><i class='material-icons left'>visibility</i></label>
            <input id='inactivo' type='radio' name='estado' class='with-gap' value='0' <?php print(($estado == 0)?"checked":""); ?>/>
            <label for='inactivo'><i class='material-icons left'>visibility_off</i></label>
        </div>
        
    </div>
    <?php
    if($id == null)
    {
    ?>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate' required/>
            <label for='clave1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate' required/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <?php
    }
    ?>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>