<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../lib/page.php");
//Se coloca el titulo
Page::header("Usuarios");

//Se utiliza una consulta para realizar el buscador
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM usuarios WHERE apellidos LIKE ? OR nombres LIKE ? ORDER BY apellidos";
	$params = array("%$search%", "%$search%");
}
else
{
	//Consulta para obtener los registros
	$sql = "SELECT * FROM usuarios ORDER BY apellidos";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- Los botones y los cuadros de texto-->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombres o apellidos'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>APELLIDOS</th>
			<th>NOMBRES</th>
			<th>CORREO</th>
			<th>ALIAS</th>
			<th>GENERO</th>
			<th>NACION</th>
			<th>ESTADO</th>		
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['apellidos']."</td>
				<td>".$row['nombres']."</td>
				<td>".$row['correo']."</td>
				<td>".$row['alias']."</td>
				<td>".$row['genero']."</td>
				<td>".$row['nacion']."</td>
				<td>
	    ");
		if($row['estado_usuario'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
		print("
				</td>
				<td>
					<a href='save.php?id=".$row['id_usuario']."' class='blue-text'><i class='material-icons'>edit</i></a>
					<a href='delete.php?id=".$row['id_usuario']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>