<?php
require("../../lib/database.php");
require("../../lib/validator.php");
class Page
{
	public static function header($title)
	{
		session_start();
		ini_set("date.timezone","America/El_Salvador");
		print("
<!DOCTYPE html>
			<html lang='es'>
			<head>
				<meta charset='utf-8'>
				<title>Dashboard - $title</title>
				<link type='text/css' rel='stylesheet' href='../../css/materialize.min.css'/>
				<link type='text/css' rel='stylesheet' href='../../css/sweetalert2.min.css'/>
				<link type='text/css' rel='stylesheet' href='../../css/icon.css'/>
				<link type='text/css' rel='stylesheet' href='../css/dashboard.css'/>
				<script type='text/javascript' src='../../js/sweetalert2.min.js'></script>
				<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
			</head>
			<body>
		");
		if(isset($_SESSION['cliente']))
		{
			print("
   <div class='navbar-fixed'>
 <nav>
    <div class='nav-wrapper blue lighten-2'>
      <a href='index.php' class='brand-logo'> <img src='../img/333.png'width='90' height='65'></a>
      <a href='#' data-activates='mobile-demo' class='button-collapse'><i class='material-icons'>menu</i></a>
      <ul class='right hide-on-med-and-down'>
        <li><a href='../../public/productos.php'><i class='material-icons left'>web</i>Inicio</a></li>
        <li><a href='../../public/index.php'><i class='material-icons left'>web</i>Mision y Vision</a></li>
        <li><a href='../../public/producs.php'><i class='material-icons left'>shop</i>Productos</a></li>
        <li><a href='../../public/contactenos.php'><i class='material-icons left'>phone</i>Contactenos</a></li>
        <a class='btn tooltipped light-blue  green darken -3 ' data-position='bottom' data-delay='50' data-tooltip='En Hora Buena' href='../main/subdetalle.php'><i class='material-icons'>shopping_cart</i></a>
        <a class='btn tooltipped light-blue  black' data-position='bottom' data-delay='50' data-tooltip='Home' href='../main/index.php'><i class='material-icons'>perm_identity</i></a>
      </ul>
      
      <ul class='side-nav' id='mobile-demo'>
        <li><a href='../../public/productos.php'>Inicio</a></li>
        <li><a href='../../public/index.php'>Mision y Vision</a></li>
        <li><a href='../../public/producs.php'>Productos</a></li>
        <li><a href='../../public/contactenos.php'>Contactenos</a></li>
        <a class='btn tooltipped light-blue yellow' data-position='bottom' data-delay='50' data-tooltip='En Hora Buena' href='../main/login.php'>Carrito</a>
            <a class='btn tooltipped light-blue  blue lighten-5' data-position='bottom' data-delay='50' data-tooltip='Home' href='../main/index.php'>Cuenta</a>
      </ul>
      </div>
      </nav>
   </div>
		<h3 class='center-align'>".$title."</h3>
    </div>
			");
		}
		else
		{
			print("
    <header class='navbar-fixed'>
					<nav class='brown'>
						<div class='nav-wrapper'>
							<a href='../main/' class='brand-logo'><i class='material-icons'>dashboard</i></a>
						</div>
					</nav>
				</header>
				<main class='container'>
			");
			$filename = basename($_SERVER['PHP_SELF']);
			if($filename != "login.php" && $filename != "nuevoregistro.php" && $filename != "detalle.php")
			{
				self::showMessage(3, "¡Debe iniciar sesión!", "../main/login.php");
				self::footer();
				exit;
			}
			else
			{
				print("<h3 class='center-align'>".$title."</h3>");
			}
		}
	}

	public static function footer()
	{
		print("
		</main>
			<footer class='page-footer black'>
				<div class='container'>
					<div class='row'>
						<div class='col s12 m6'>
							<h5 class='white-text'>Dashboard</h5>
							<a class='white-text' href='mailto:gerencia@paramedicos.com.sv'><i class='material-icons left'>email</i>Ayuda</a>
						</div>
						<div class='col s12 m6'>
							<h5 class='white-text'>Enlaces</h5>
							<a class='white-text' href='../../public/' target='_blank'><i class='material-icons left'>store</i>Sitio público</a>
						</div>
					</div>
				</div>
				<div class='footer-copyright'>
					<div class='container'>
						<span>©".date(' Y ')."Paramedicos El Salvador C.A, todos los derechos reservados.</span>
						<span class='white-text right'>Diseñado con <a class='red-text text-accent-1' href='http://materializecss.com/' target='_blank'><b>Materialize</b></a></span>
					</div>
				</div>
			</footer>
			<script type='text/javascript' src='../../js/jquery-2.1.1.min.js'></script>
			<script type='text/javascript' src='../../js/materialize.min.js'></script>
			<script type='text/javascript' src='../js/dashboard.js'></script>
			</body>
			</html>
		");
	}

	public static function setCombo($label, $name, $value, $query)
	{
		$data = Database::getRows($query, null);
		print("<select name='$name' required>");
		if($data != null)
		{
			if($value == null)
			{
				print("<option value='' disabled selected>Seleccione una opción</option>");
			}
			foreach($data as $row)
			{
				if(isset($_POST[$name]) == $row[0] || $value == $row[0])
				{
					print("<option value='$row[0]' selected>$row[1]</option>");
				}
				else
				{
					print("<option value='$row[0]'>$row[1]</option>");
				}
			}
		}
		else
		{
			print("<option value='' disabled selected>No hay registros</option>");
		}
		print("
			</select>
			<label>$label</label>
		");
	}

	public static function showMessage($type, $message, $url)
	{
		if(is_numeric($message))
		{
			switch($message)
			{
				case 1045:
					$text = "Autenticación desconocida";
					break;
				case 1049:
					$text = "Base de datos desconocida";
					break;
				case 1054:
					$text = "Nombre de campo desconocido";
					break;
				case 1062:
					$text = "Dato duplicado, no se puede guardar";
					break;
				case 1146:
					$text = "Nombre de tabla desconocido";
					break;
				case 1451:
					$text = "Registro ocupado, no se puede eliminar";
					break;
				case 2002:
					$text = "Servidor desconocido";
					break;
				default:
					$text = "Ocurrio un problema, contacte al administrador :(";
			}
		}
		else
		{
			$text = $message;
		}

		switch($type)
		{
			case 1:
				$title = "Éxito";
				$icon = "success";
				break;
			case 2:
				$title = "Error";
				$icon = "error";
				break;
			case 3:
				$title = "Advertencia";
				$icon = "warning";
				break;
			case 4:
				$title = "Aviso";
				$icon = "info";
		}

		if($url != null)
		{
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false}).then(function(){location.href = '$url'})</script>");
		}
		else
		{
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false})</script>");
		}
	}
}
?>