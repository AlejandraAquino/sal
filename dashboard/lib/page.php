<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../../lib/database.php");
require("../../lib/validator.php");
//Se declara la clase para el diseño
class Page
{
	//Se declara la funcion del titulo
	public static function header($title)
	{
		//Se inicia sesion y muestra el tiempo de EL Salvador
		session_start();
		ini_set("date.timezone","America/El_Salvador");
		//Carga los archivos css y js
		print("
			<!DOCTYPE html>
			<html lang='es'>
			<head>
				<meta charset='utf-8'>
				<title>Dashboard - $title</title>
				<link type='text/css' rel='stylesheet' href='../../css/materialize.min.css'/>
				<link type='text/css' rel='stylesheet' href='../../css/sweetalert2.min.css'/>
				<link type='text/css' rel='stylesheet' href='../../css/icon.css'/>
				<link type='text/css' rel='stylesheet' href='../css/dashboard.css'/>
				<script type='text/javascript' src='../../js/sweetalert2.min.js'></script>
				<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
			</head>
			<body>
		");
		//Si inicia sesion que muestre todos los mantenimientos
		if(isset($_SESSION['nombre_usuario']))
		{
			print("
				<header class='navbar-fixed'>
					<nav class='blue'>
						<div class='nav-wrapper'>
							<a href='../main/' class='brand-logo'><i class='material-icons left hide-on-med-and-down'>dashboard</i></a>
							<a href='#' data-activates='mobile' class='button-collapse'><i class='material-icons'>menu</i></a>
							<ul class='right hide-on-med-and-down'>
								<li><a href='../products/'><i class='material-icons left'>shop</i>Productos</a></li>
								<li><a href='../Compras/compras.php'><i class='material-icons left'>shopping_basket</i>Compras</a></li>
					            <li><a href='../Comentarios/comen.php'><i class='material-icons left'>chat_bubble_outline</i>Comentarios</a></li>
								<li><a href='../marca/'><i class='material-icons left'>stars</i>Distribuidoras</a></li>
								<li><a href='../categorias/'><i class='material-icons left'>shop_two</i>Categorías</a></li>
								<li><a href='../admin/'><i class='material-icons left'>supervisor_account</i>Usuarios</a></li>
								<li><a class='dropdown-button' href='#' data-activates='dropdown'><i class='material-icons left'>verified_user</i>".$_SESSION['nombre_usuario']."</a></li>
							</ul>
							<ul id='dropdown' class='dropdown-content'>
								<li><a href='../main/profile.php'><i class='material-icons left'>edit</i>Editar perfil</a></li>
								<li><a href='../main/logout.php'><i class='material-icons left'>clear</i>Salir</a></li>
							</ul>
						</div>
					</nav>
				</header>
				<ul class='side-nav' id='mobile'>
							    <li><a href='../products/'><i class='material-icons left'>shop</i>Productos</a></li>
								<li><a href='../marca/'><i class='material-icons left'>stars</i>Distribuidoras</a></li>
								<li><a href='../categorias/'><i class='material-icons left'>shop_two</i>Categorías</a></li>
								<li><a href='../products/'><i class='material-icons left'>shop</i>Productos</a></li>
								<li><a href='../Compras/compras.php'><i class='material-icons left'>shopping_basket</i>Compras</a></li>
					            <li><a href='../Comentarios/comen.php'><i class='material-icons left'>chat_bubble_outline</i>Comentarios</a></li>
								<li><a href='../admin/'><i class='material-icons left'>supervisor_account</i>Usuarios</a></li>
					            <li><a class='dropdown-button' href='#' data-activates='dropdown-mobile'><i class='material-icons'>verified_user</i>".$_SESSION['nombre_usuario']."</a></li>
				</ul>
				<ul id='dropdown-mobile' class='dropdown-content'>
					<li><a href='../main/profile.php'>Editar perfil</a></li>
					<li><a href='../main/logout.php'>Salir</a></li>
				</ul>
				<main class='container'>
					<h3 class='center-align'>".$title."</h3>
			");
			}
		else
		{
			if(isset($_SESSION['cliente']))
			{
			print("
        <nav>
            <div class='nav-wrapper blue lighten -2'>
                <a href='index.1.php' class='brand-logo left'><img src='../../img/333.png' width='100px'> </a>
                <a href='#' data-activates='mobile-demo' class='button-collapse'><span class='icon-menu'></span></a>
                <ul class='right hide-on-med-and-down'>

                    <li><a href='../main/miscompras.php'><i class='material-icons left'>shop</i>Compras</a></li>
                    <li><a href='../main/perfilcliente.php'><i class='material-icons left'>verified_user</i>".$_SESSION['cliente']."</a></li>
                    <li><a href='../main/logout.php'><i class='material-icons left'>input</i> Cerrar Sesion</a></li>
					<li><a href='../../public/index.php'><i class='material-icons left'>web</i>Inicio</a></li> 
                </ul>
            </div>
        </nav>
        <!-- Navbar para menu de pantallas pequeñás-->
        <ul class='side-nav blue darken-4' id='mobile-demo'>
            <a class='btn tooltipped light-blue' data-position='bottom' data-delay='50' data-tooltip='¡Unete!' href='login.php'>Log In</a>
                    
                    <li><a href='../main/miscompras.php'>Compras</a></li>
                    <li><a href='../main/perfilcliente.php'>".$_SESSION['cliente']."</a></li>
                    <li><a href='../main/logout.php'>Cerrar Sesion</a></li>
					<li><a href='../../public/index.php'>Inicio</a></li>
        </ul>
	<h3 class='center-align'>".$title."</h3>
			");
		}
		else
		{
			print("
				<header class='navbar-fixed'>
					<nav class='brown'>
						<div class='nav-wrapper'>
							<a href='../main/' class='brand-logo'><i class='material-icons'>dashboard</i></a>
						</div>
					</nav>
				</header>
				<main class='container'>
			");
			//Tiene que iniciar sesion para acceder
			$filename = basename($_SERVER['PHP_SELF']);
			if($filename != "login.php" && $filename != "register.php")
			{
				self::showMessage(3, "¡Debe iniciar sesión!", "../main/login.php");
				self::footer();
				exit;
			}
			else
			{
				print("<h3 class='center-align'>".$title."</h3>");
			}
		}
	}
	}
    //Funcion para el diseño del footer
	public static function footer()
	{
		print("
			</main>
			<footer class='page-footer black'>
				<div class='container'>
					<div class='row'>
						<div class='col s12 m6'>
							<h5 class='white-text'>Dashboard</h5>
							<a class='white-text' href='mailto:gerencia@paramedicos.com.sv'><i class='material-icons left'>email</i>Ayuda</a>
						</div>
						<div class='col s12 m6'>
							<h5 class='white-text'>Enlaces</h5>
							<a class='white-text' href='../../public/' target='_blank'><i class='material-icons left'>store</i>Sitio público</a>
						</div>
					</div>
				</div>
				<div class='footer-copyright'>
					<div class='container'>
						<span>©".date(' Y ')."Paramedicos El Salvador C.A, todos los derechos reservados.</span>
						<span class='white-text right'>Diseñado con <a class='red-text text-accent-1' href='http://materializecss.com/' target='_blank'><b>Materialize</b></a></span>
					</div>
				</div>
			</footer>
			<script type='text/javascript' src='../../js/jquery-2.1.1.min.js'></script>
			<script type='text/javascript' src='../../js/materialize.min.js'></script>
			<script type='text/javascript' src='../js/dashboard.js'></script>
			</body>
			</html>
		");
	}
    
	//Funcion para obtener el combobox
	public static function setCombo($label, $name, $value, $query)
	{
	    //Se obtienen los registros
		$data = Database::getRows($query, null);
		//Imprime el nombre seleccionado
		print("<select name='$name' required>");
		//
		if($data != null)
		{
			if($value == null)
			{
				print("<option value='' disabled selected>Seleccione una opción</option>");
			}
			foreach($data as $row)
			{
				if(isset($_POST[$name]) == $row[0] || $value == $row[0])
				{
					//Imprime la opcion seleccionada
					print("<option value='$row[0]' selected>$row[1]</option>");
				}
				else
				{
					print("<option value='$row[0]'>$row[1]</option>");
				}
			}
		}
		else
		{
			//En caso de que no existan registros
			print("<option value='' disabled selected>No hay registros</option>");
		}
		print("
			</select>
			<label>$label</label>
		");
	}
	//public static function setFecha($f, $fecha, $fecha_sql)
	//{
		//$f = explode('/', $fecha)
		//$fecha_sql = $f[2]."-".$f[0]."-".$f[1];
		//print(
			//"<label>$fecha_sql</label>"
		//)
	//}


    //Fucnion para mostrar mensaje de alerta
	public static function showMessage($type, $message, $url)
	{
		if(is_numeric($message))
		{
			switch($message)
			{
				case 1045:
					$text = "Autenticación desconocida";
					break;
				case 1049:
					$text = "Base de datos desconocida";
					break;
				case 1054:
					$text = "Nombre de campo desconocido";
					break;
				case 1062:
					$text = "Dato duplicado, no se puede guardar";
					break;
				case 1146:
					$text = "Nombre de tabla desconocido";
					break;
				case 1451:
					$text = "Registro ocupado, no se puede eliminar";
					break;
				case 2002:
					$text = "Servidor desconocido";
					break;
				default:
					$text = "Ocurrio un problema, contacte al administrador :(";
			}
		}
		else
		{
			$text = $message;
		}
		switch($type)
		{
			//Caso de exito
			case 1:
				$title = "Éxito";
				$icon = "success";
				break;
			//Caso de error
			case 2:
				$title = "Error";
				$icon = "error";
				break;
			case 3:
			//Caso de advertencia
				$title = "Advertencia";
				$icon = "warning";
				break;
			//Caso de aviso
			case 4:
				$title = "Aviso";
				$icon = "info";
		}
		if($url != null)
		{
			//Imprime
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false}).then(function(){location.href = '$url'})</script>");
		}
		else
		{
			//Imprime
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false})</script>");
		}
	}
}
?>