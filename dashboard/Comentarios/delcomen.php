<?php
require("../lib/page.php");
Page::header("Eliminar Comentario");

if(!empty($_GET['id'])) 
{
    $id = $_GET['id'];
}
else
{
    header("location: comen.php");
}
if(!empty($_POST))
{
    $id = $_POST['id'];
    try
    {
            $sql = "DELETE FROM comentario WHERE Id_Comentario = ?";
		    $params = array($id);
	    if(Database::executeRow($sql, $params))
		{
			Page::showMessage(1, "Operación satisfactoria", "comen.php");
		}
        else
        {
            throw new Exception(Database::$error[1]);
        }
    }
    	catch (Exception $error) 
	{
		Page::showMessage(2, $error->getMessage(), "comen.php");
	}
}
?>
<form method='post'>
	<div class='row center-align'>
		<input type='hidden' name='id' value='<?php print($id); ?>'/>
		<button type='submit' class='btn waves-effect red'><span class="icon-point-down"> Borrar</span></button>
		<a href='comen.php' class='btn waves-effect grey'><span class="icon-point-down"> Cancelar</span></a>
	</div>
</form>
<?php
Page::footer();
?>

