<?php
require("../lib/page.php");
Page::header("Mis Compras");
?>
<?php
    $sql5 = "SELECT comentario.Id_Comentario,comentario.Comentario, clientes.Usuario_Cliente FROM comentario, clientes, productos WHERE comentario.Id_Cliente=clientes.Id_Cliente GROUP BY comentario.Id_Comentario";
    $data = Database::getRows($sql5,null);
    try
    {
        
        if($data != null)
        {
            print("
            <div class='container'>
<table class='striped centered' >
	<thead>
		<tr>
			<th>Comentario</th>
			<th>Usuario Cliente</th>
			<th>Eliminar</th>

		</tr>
	</thead>
	<tbody>
");
            foreach($data as $row)
            {
                        print("
			<tr>
				<td>".$row['Comentario']."</td>
				<td>".$row['Usuario_Cliente']."</td>
				<td>
                <a href='delcomen.php?id=".$row['Id_Comentario']."' class='red-text'><spa class='icon-blocked'></span>eliminar</a>
				</td>
			</tr>
		            ");
            }
                	print("
                </tbody>
            </table>
            </div>
            ");
        }
        else
        {
        Page::showMessage(4, "No hay comentarios", "../main/index.php");
        }
    }
    catch(Exception $error)
{
	Page::showMessage(2, $error->getMessage(), "../main/");
}
Page::footer();
?>
