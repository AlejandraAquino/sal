<?php
require("../lib/page.php");
Page::header("Mis Compras");
?>
<?php
    $sql = "SELECT f.Id_Factura, f.Fecha_Factura, p.nombre_producto, p.id_producto, p.precio_producto, v.Cantidad_Venta, c.Nombres_Cliente,c.Apellidos_Cliente,c.Direccion_Cliente, c.Telefono_Cliente FROM ventas v, factura f, productos p, clientes c WHERE f.Estado_Factura= ? AND v.id_producto= p.id_producto AND f.Id_Factura=v.Id_Factura GROUP BY v.Id_Venta";
    $es = 0; //varibale que solo mostraran las comrpas que si se compraron
    $params = array($es);
    $data = Database::getRows($sql, $params);
    try
    {
        
        if($data != null)
        {
            print("
            <div class='container'>
<table class='striped centered' >
	<thead>
		<tr>
			<th>No Factura</th>
			<th>Fecha</th>
			<th>Productos</th>
            <th>No Producto</th>
			<th>Precio</th>
            <th>Cantidad</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Direccion</th>
            <th>Telefono</th>
		</tr>
	</thead>
	<tbody>
");
            foreach($data as $row)
            {
                        print("
			<tr>
				<td>".$row['Id_Factura']."</td>
				<td>".$row['Fecha_Factura']."</td>
				<td>".$row['nombre_producto']."</td>
                <td>".$row['id_producto']."</td>            
				<td>".$row['precio_producto']."</td>
                <td>".$row['Cantidad_Venta']."</td>
                <td>".$row['Nombres_Cliente']."</td>
                <td>".$row['Apellidos_Cliente']."</td>
                <td>".$row['Direccion_Cliente']."</td>
                <td>".$row['Telefono_Cliente']."</td>
                
			</tr>
		            ");
            }
                	print("
                </tbody>
            </table>
            </div>
            ");
        }
        else
        {
        Page::showMessage(4, "No has realizado compras", "../main/index.php");
        }
    }
    catch(Exception $error)
{
	Page::showMessage(2, $error->getMessage(), "../main/");
}
Page::footer();
?>
