<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../lib/page.php");
if(empty($_GET['id'])) 
{
    Page::header("Agregar producto");
    //Se declaran la variables
    $id = null;
    $nombre = null;
    $descripcion = null;
    $precio = null;
    $imagen = null;
    $estado = 1;
    $categoria = null;
    $marca = null;
}
else
{
    Page::header("Modificar producto");
    //Se obtiene el Id del dato guardado
    $id = $_GET['id'];
    $sql = "SELECT * FROM productos WHERE id_producto = ?";
    $params = array($id);
    //Se obtienen los datos de todas la columnas
    $data = Database::getRow($sql, $params);
    $nombre = $data['nombre_producto'];
    $descripcion = $data['descripcion_producto'];
    $precio = $data['precio_producto'];
    $cantidad = $data['cantidad'];
    $imagen = $data['imagen_producto'];
    $estado = $data['estado_producto'];
    $categoria = $data['id_tipoproducto'];
    $marca = $data['id_marca'];
    
}

if(!empty($_POST))
{
    //Se validan los datos
    $_POST = Validator::validateForm($_POST);
  	$nombre = $_POST['nombre'];
  	$descripcion = $_POST['descripcion'];
    $precio = $_POST['precio'];
    $cantidad = $_POST['cantidad'];
    $archivo = $_FILES['imagen'];
    $estado = $_POST['estado'];
    $categoria = $_POST['categoria'];
    $marca = $_POST['marca'];

    try 
    {
        if($nombre != "")
        {
            if($precio != "")
            {
                if($precio > 0)
                {
                    if($descripcion != "")
                    {
                        if($categoria != "")
                        {
                            //Validador de imagen
                            if($archivo['name'] != null)
                            {
                                $imagen = Validator::validateImage($archivo, 1200, 1200);
                            }
                            else
                            {
                                if($imagen == null)
                                {
                                    throw new Exception("Debe seleccionar una imagen");
                                }
                            }
                            if($id == null)
                            {
                                //consulta para guardar
                                $sql = "INSERT INTO productos(nombre_producto, descripcion_producto, precio_producto, cantidad, imagen_producto, estado_producto, id_tipoproducto, id_marca) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
                                 //Se ponen los parametros
                                $params = array($nombre, $descripcion, $precio, $cantidad, $imagen, $estado, $categoria, $marca);
                            }
                            else
                            {
                                //consulta para actualizar los registros
                                $sql = "UPDATE productos SET nombre_producto = ?, descripcion_producto = ?, precio_producto = ?, cantidad = ?, imagen_producto = ?, estado_producto = ?, id_tipoproducto = ?, id_marca= ? WHERE id_producto = ?";
                                 //Se ponen los parametros
                                $params = array($nombre, $descripcion, $precio, $cantidad, $imagen, $estado, $categoria, $marca, $id);
                            }
                            if(Database::executeRow($sql, $params))
                            {
                                Page::showMessage(1, "Operación satisfactoria", "index.php");
                            }
                            else
                            {
                                throw new Exception("Operación fallida");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe seleccionar una categoría");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe digitar una descripción");
                    }
                }
                else
                {
                    throw new Exception("El precio debe ser mayor que 0.00");
                }
            }
            else
            {
                throw new Exception("Debe ingresar el precio");
            }
        }
        else
        {
            throw new Exception("Debe digitar el nombre");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<!-- Botones y cuadros de texto -->
<form method='post' enctype='multipart/form-data'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>note_add</i>
          	<input id='nombre' type='text' name='nombre' class='validate' value='<?php print($nombre); ?>' required/>
          	<label for='nombre'>Nombre</label>
        </div>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>shopping_cart</i>
          	<input id='precio' type='number' name='precio' class='validate' max='999.99' min='0.01' step='any' value='<?php print($precio); ?>' required/>
          	<label for='precio'>Precio ($)</label>
        </div>
        <div class='input-field col s12 m6'>
          	<input id='precio' type='number' name='cantidad' class='validate' max='999.99' min='0.01' step='any' value='<?php print($cantidad); ?>' required/>
          	<label for='precio'>cantidad</label>
        </div>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>description</i>
          	<input id='descripcion' type='text' name='descripcion' class='validate' value='<?php print($descripcion); ?>'/>
          	<label for='descripcion'>Descripción</label>
        </div>
        <div class='input-field col s12 m6'>
            <?php
            $sql = "SELECT id_tipoproducto, nombre_categoria FROM tipoproducto";
            Page::setCombo("Categoría", "categoria", $categoria, $sql);
            ?>
        </div>
        <div class='input-field col s12 m6'>
            <?php
            $sql = "SELECT id_marca, nombre_marca FROM distribucion";
            Page::setCombo("Marca", "marca", $marca, $sql);
            ?>
        </div>
      	<div class='file-field input-field col s12 m6'>
            <div class='btn waves-effect'>
                <span><i class='material-icons'>image</i></span>
                <input type='file' name='imagen' <?php print(($imagen == null)?"required":""); ?>/>
            </div>
            <div class='file-path-wrapper'>
                <input class='file-path validate' type='text' placeholder='Seleccione una imagen'/>
            </div>
        </div>
        <div class='input-field col s12 m6'>
            <span>Estado:</span>
            <input id='activo' type='radio' name='estado' class='with-gap' value='1' <?php print(($estado == 1)?"checked":""); ?>/>
            <label for='activo'><i class='material-icons left'>visibility</i></label>
            <input id='inactivo' type='radio' name='estado' class='with-gap' value='0' <?php print(($estado == 0)?"checked":""); ?>/>
            <label for='inactivo'><i class='material-icons left'>visibility_off</i></label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>