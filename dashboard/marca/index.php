<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../lib/page.php");
Page::header("Distribuciones");

//Se utiliza una consulta para realizar el buscador
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM distribucion WHERE nombre_marca LIKE ? ORDER BY nombre_marca";
	$params = array("%$search%");
}
else
{
	//Consulta para obtener los registros
	$sql = "SELECT * FROM distribucion ORDER BY nombre_marca";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- Los botones y los cuadros de texto-->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombre'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<!-- Titulos de las columnas de la tabla-->
<table class='striped'>
	<thead>
		<tr>
			<th>NOMBRE</th>
			<th>DESCRIPCIÓN</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>
<!--Se mandan a llamar los datos de la base-->
<?php
	foreach($data as $row)
	{
		print("
			<tr>
			    <td>".$row['nombre_marca']."</td>
				<td>".$row['des_marca']."</td>
				<td>
					<a href='save.php?id=".$row['id_marca']."' class='blue-text'><i class='material-icons'>edit</i></a>
					<a href='delete.php?id=".$row['id_marca']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>