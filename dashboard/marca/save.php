<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../lib/page.php");
if(empty($_GET['id'])) 
{
    Page::header("Agregar marca");
    //Se declaran la variables
    $id = null;
    $nombre = null;
    $descripcion = null;
}
else
{
    Page::header("Modificar marca");
    //Se obtiene el Id del dato guardado
    $id = $_GET['id'];
    $sql = "SELECT * FROM distribucion WHERE id_marca = ?";
    $params = array($id);
    //Se obtienen los datos de todas la columnas
    $data = Database::getRow($sql, $params);
    $nombre = $data['nombre_marca'];
    $descripcion = $data['des_marca'];
}

if(!empty($_POST))
{    
    //Se validan los datos
    $_POST = Validator::validateForm($_POST);
  	$nombre = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];
    if($descripcion == "")
    {
        $descripcion = null;
    }
    try 
    {
      	if($nombre != "")
        {
            if($id == null)
            {
                $sql = "INSERT INTO distribucion(nombre_marca, des_marca) VALUES(?,?)";
                $params = array($nombre, $descripcion);
            }
            else
            {
                //consulta para actualizar los registros
                $sql = "UPDATE distribucion SET nombre_marca = ?, des_marca = ? WHERE id_marca = ?";
                //Se ponen los parametros
                $params = array($nombre, $descripcion, $id);
            }
            if(Database::executeRow($sql, $params))
            {
                Page::showMessage(1, "Operación satisfactoria", "index.php");
            }
            else
            {
                throw new Exception("Operación fallida");
            }
        }
        else
        {
            throw new Exception("Debe digitar un nombre");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<!-- Botones y cuadros de texto -->
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>note_add</i>
            <input id='nombre' type='text' name='nombre' class='validate' value='<?php print($nombre); ?>' required/>
            <label for='nombre'>Nombre</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>description</i>
            <input id='descripcion' type='text' name='descripcion' class='validate' value='<?php print($descripcion); ?>'/>
            <label for='descripcion'>Descripción</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>