<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../lib/page.php");
//Se coloca el titulo
Page::header("Administradores");

//Se utiliza una consulta para realizar el buscador
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM administrador WHERE apellido_admin LIKE ? OR nombre_admin LIKE ? ORDER BY apellido_admin";
	$params = array("%$search%", "%$search%");
}
else
{
	//Consulta para obtener los registros
	$sql = "SELECT * FROM administrador ORDER BY apellido_admin";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- Los botones y los cuadros de texto-->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn waves-effect green'><i class='material-icons'>check_circle</i></button> 	
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<!-- Titulos de las columnas de la tabla-->
<table class='striped'>
	<thead>
		<tr>
			<th>APELLIDOS</th>
			<th>NOMBRES</th>
			<th>CORREO</th>
			<th>ALIAS</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<!--Se mandan a llamar los datos de la base-->
<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['apellido_admin']."</td>
				<td>".$row['nombre_admin']."</td>
				<td>".$row['correo']."</td>
				<td>".$row['alias']."</td>
				<td>
					<a href='save.php?id=".$row['id_admin']."' class='blue-text'><i class='small material-icons'>edit</i></a>
					<a href='delete.php?id=".$row['id_admin']."' class='red-text'><i class='small material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>